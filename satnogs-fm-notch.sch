EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "SatNOGS FM Notch Filter"
Date "2017-02-20"
Rev "v1"
Comp "Libre Space Foundation"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_Coaxial P2
U 1 1 58AAC85E
P 7450 3200
F 0 "P2" H 7460 3320 50  0000 C CNN
F 1 "Conn_RF" V 7560 3140 50  0000 C CNN
F 2 "Connector_Coaxial:SMA_Amphenol_132289_EdgeMount" H 7450 3200 50  0001 C CNN
F 3 "" H 7450 3200 50  0000 C CNN
F 4 "73251-1150" H 7450 3200 60  0001 C CNN "Mnf."
	1    7450 3200
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_Coaxial P1
U 1 1 58AAC8EE
P 3700 3200
F 0 "P1" H 3710 3320 50  0000 C CNN
F 1 "Conn_RF" V 3810 3140 50  0000 C CNN
F 2 "Connector_Coaxial:SMA_Amphenol_132289_EdgeMount" H 3700 3200 50  0001 C CNN
F 3 "" H 3700 3200 50  0000 C CNN
F 4 "73251-1150" H 3700 3200 60  0001 C CNN "Mnf."
	1    3700 3200
	-1   0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 58AAC968
P 4200 3650
F 0 "C1" H 4225 3750 50  0000 L CNN
F 1 "6.8p" H 4225 3550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4238 3500 50  0001 C CNN
F 3 "" H 4200 3650 50  0000 C CNN
F 4 "GRM1885C1H6R8DA01D" H 4200 3650 60  0001 C CNN "Mnf."
	1    4200 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 58AACA68
P 5200 3650
F 0 "C3" H 5225 3750 50  0000 L CNN
F 1 "15pF" H 5225 3550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5238 3500 50  0001 C CNN
F 3 "" H 5200 3650 50  0000 C CNN
F 4 "GRM1885C1H150JA01D" H 5200 3650 60  0001 C CNN "Mnf."
	1    5200 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 58AACAB0
P 6150 3650
F 0 "C5" H 6175 3750 50  0000 L CNN
F 1 "15pF" H 6175 3550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6188 3500 50  0001 C CNN
F 3 "" H 6150 3650 50  0000 C CNN
F 4 "GRM1885C1H150JA01D" H 6150 3650 60  0001 C CNN "Mnf."
	1    6150 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 58AACAF5
P 7050 3650
F 0 "C7" H 7075 3750 50  0000 L CNN
F 1 "6.8pF" H 7075 3550 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 7088 3500 50  0001 C CNN
F 3 "" H 7050 3650 50  0000 C CNN
F 4 "GRM1885C1H6R8DA01D" H 7050 3650 60  0001 C CNN "Mnf."
	1    7050 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:L L1
U 1 1 58AACB35
P 4200 4100
F 0 "L1" V 4150 4100 50  0000 C CNN
F 1 "330nF" V 4275 4100 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 4200 4100 50  0001 C CNN
F 3 "" H 4200 4100 50  0000 C CNN
F 4 "LQW18CNR33J00" V 4200 4100 60  0001 C CNN "Mnf."
	1    4200 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:L L3
U 1 1 58AACC49
P 5200 4100
F 0 "L3" V 5150 4100 50  0000 C CNN
F 1 "180nH" V 5275 4100 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 5200 4100 50  0001 C CNN
F 3 "" H 5200 4100 50  0000 C CNN
F 4 "0603AS-R18J-YY" V 5200 4100 60  0001 C CNN "Mnf."
	1    5200 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:L L7
U 1 1 58AACCEE
P 7050 4100
F 0 "L7" V 7000 4100 50  0000 C CNN
F 1 "330nF" V 7125 4100 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 7050 4100 50  0001 C CNN
F 3 "" H 7050 4100 50  0000 C CNN
F 4 "LQW18CNR33J00" V 7050 4100 60  0001 C CNN "Mnf."
	1    7050 4100
	1    0    0    -1  
$EndComp
$Comp
L Device:L L2
U 1 1 58AACD3C
P 4700 3100
F 0 "L2" V 4650 3100 50  0000 C CNN
F 1 "22nH" V 4775 3100 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 4700 3100 50  0001 C CNN
F 3 "" H 4700 3100 50  0000 C CNN
F 4 "0603AS-022J-YY" V 4700 3100 60  0001 C CNN "Mnf."
	1    4700 3100
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L4
U 1 1 58AACE15
P 5700 3100
F 0 "L4" V 5650 3100 50  0000 C CNN
F 1 "27nF" V 5775 3100 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 5700 3100 50  0001 C CNN
F 3 "" H 5700 3100 50  0000 C CNN
F 4 "0603AS-027J-YY" V 5700 3100 60  0001 C CNN "Mnf."
	1    5700 3100
	0    -1   -1   0   
$EndComp
$Comp
L Device:L L6
U 1 1 58AACE73
P 6650 3100
F 0 "L6" V 6600 3100 50  0000 C CNN
F 1 "22nH" V 6725 3100 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 6650 3100 50  0001 C CNN
F 3 "" H 6650 3100 50  0000 C CNN
F 4 "0603AS-022J-YY" V 6650 3100 60  0001 C CNN "Mnf."
	1    6650 3100
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C2
U 1 1 58AACED4
P 4700 3350
F 0 "C2" H 4725 3450 50  0000 L CNN
F 1 "100pF" H 4725 3250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 4738 3200 50  0001 C CNN
F 3 "" H 4700 3350 50  0000 C CNN
F 4 "GRM1885C1H101JA01D" H 4700 3350 60  0001 C CNN "Mnf."
	1    4700 3350
	0    1    1    0   
$EndComp
$Comp
L Device:C C4
U 1 1 58AACF4A
P 5700 3350
F 0 "C4" H 5725 3450 50  0000 L CNN
F 1 "100pF" H 5725 3250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 5738 3200 50  0001 C CNN
F 3 "" H 5700 3350 50  0000 C CNN
F 4 "GRM1885C1H101JA01D" H 5700 3350 60  0001 C CNN "Mnf."
	1    5700 3350
	0    1    1    0   
$EndComp
$Comp
L Device:C C6
U 1 1 58AACFB3
P 6650 3350
F 0 "C6" H 6675 3450 50  0000 L CNN
F 1 "100pF" H 6675 3250 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric" H 6688 3200 50  0001 C CNN
F 3 "" H 6650 3350 50  0000 C CNN
F 4 "GRM1885C1H101JA01D" H 6650 3350 60  0001 C CNN "Mnf."
	1    6650 3350
	0    1    1    0   
$EndComp
Wire Wire Line
	4200 3950 4200 3800
Wire Wire Line
	4200 3200 4200 3500
Wire Wire Line
	5550 3350 5500 3350
Wire Wire Line
	5500 3350 5500 3200
Wire Wire Line
	5500 3100 5550 3100
Wire Wire Line
	5850 3100 5900 3100
Wire Wire Line
	5900 3100 5900 3200
Wire Wire Line
	5900 3350 5850 3350
Wire Wire Line
	6500 3350 6450 3350
Wire Wire Line
	6450 3350 6450 3200
Wire Wire Line
	6450 3100 6500 3100
Wire Wire Line
	6800 3100 6850 3100
Wire Wire Line
	6850 3100 6850 3200
Wire Wire Line
	6850 3350 6800 3350
Wire Wire Line
	7050 3800 7050 3950
Wire Wire Line
	6150 3800 6150 3950
Wire Wire Line
	5200 3800 5200 3950
Wire Wire Line
	4200 4250 4200 4300
Wire Wire Line
	5200 4250 5200 4300
Wire Wire Line
	6150 4250 6150 4300
Wire Wire Line
	7050 4250 7050 4300
Wire Wire Line
	4550 3100 4500 3100
Wire Wire Line
	4500 3100 4500 3200
Wire Wire Line
	4500 3350 4550 3350
Wire Wire Line
	4850 3350 4900 3350
Wire Wire Line
	4900 3350 4900 3200
Wire Wire Line
	4900 3100 4850 3100
Wire Wire Line
	4900 3200 5200 3200
Connection ~ 5500 3200
Connection ~ 4900 3200
Wire Wire Line
	5900 3200 6150 3200
Connection ~ 6450 3200
Connection ~ 5900 3200
Wire Wire Line
	6850 3200 7050 3200
Connection ~ 6850 3200
Wire Wire Line
	7050 3500 7050 3200
Wire Wire Line
	6150 3500 6150 3200
Connection ~ 6150 3200
Wire Wire Line
	5200 3500 5200 3200
Connection ~ 5200 3200
Connection ~ 4500 3200
$Comp
L power:GND #PWR01
U 1 1 58AAEA5B
P 4200 4300
F 0 "#PWR01" H 4200 4050 50  0001 C CNN
F 1 "GND" H 4200 4150 50  0000 C CNN
F 2 "" H 4200 4300 50  0000 C CNN
F 3 "" H 4200 4300 50  0000 C CNN
	1    4200 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 58AAEB1D
P 5200 4300
F 0 "#PWR02" H 5200 4050 50  0001 C CNN
F 1 "GND" H 5200 4150 50  0000 C CNN
F 2 "" H 5200 4300 50  0000 C CNN
F 3 "" H 5200 4300 50  0000 C CNN
	1    5200 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR03
U 1 1 58AAEC48
P 6150 4300
F 0 "#PWR03" H 6150 4050 50  0001 C CNN
F 1 "GND" H 6150 4150 50  0000 C CNN
F 2 "" H 6150 4300 50  0000 C CNN
F 3 "" H 6150 4300 50  0000 C CNN
	1    6150 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 58AAED43
P 7050 4300
F 0 "#PWR04" H 7050 4050 50  0001 C CNN
F 1 "GND" H 7050 4150 50  0000 C CNN
F 2 "" H 7050 4300 50  0000 C CNN
F 3 "" H 7050 4300 50  0000 C CNN
	1    7050 4300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 58AAEDAE
P 7450 3500
F 0 "#PWR05" H 7450 3250 50  0001 C CNN
F 1 "GND" H 7450 3350 50  0000 C CNN
F 2 "" H 7450 3500 50  0000 C CNN
F 3 "" H 7450 3500 50  0000 C CNN
	1    7450 3500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 58AAEE69
P 3700 3500
F 0 "#PWR06" H 3700 3250 50  0001 C CNN
F 1 "GND" H 3700 3350 50  0000 C CNN
F 2 "" H 3700 3500 50  0000 C CNN
F 3 "" H 3700 3500 50  0000 C CNN
	1    3700 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 3500 7450 3400
Wire Wire Line
	3700 3500 3700 3400
$Comp
L power:PWR_FLAG #FLG07
U 1 1 58AAF1E0
P 3100 3300
F 0 "#FLG07" H 3100 3395 50  0001 C CNN
F 1 "PWR_FLAG" H 3100 3480 50  0000 C CNN
F 2 "" H 3100 3300 50  0000 C CNN
F 3 "" H 3100 3300 50  0000 C CNN
	1    3100 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 58AAF2F2
P 3100 3500
F 0 "#PWR08" H 3100 3250 50  0001 C CNN
F 1 "GND" H 3100 3350 50  0000 C CNN
F 2 "" H 3100 3500 50  0000 C CNN
F 3 "" H 3100 3500 50  0000 C CNN
	1    3100 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 3500 3100 3300
Wire Wire Line
	5500 3200 5500 3100
Wire Wire Line
	4900 3200 4900 3100
Wire Wire Line
	6450 3200 6450 3100
Wire Wire Line
	5900 3200 5900 3350
Wire Wire Line
	6850 3200 6850 3350
Wire Wire Line
	6150 3200 6450 3200
Wire Wire Line
	5200 3200 5500 3200
Wire Wire Line
	4500 3200 4500 3350
Wire Wire Line
	4200 3200 4500 3200
$Comp
L Device:L L5
U 1 1 58AACCA1
P 6150 4100
F 0 "L5" V 6100 4100 50  0000 C CNN
F 1 "180nF" V 6225 4100 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric" H 6150 4100 50  0001 C CNN
F 3 "" H 6150 4100 50  0000 C CNN
F 4 "0603AS-R18J-YY" V 6150 4100 60  0001 C CNN "Mnf."
	1    6150 4100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 3200 7250 3200
Connection ~ 7050 3200
Wire Wire Line
	4200 3200 3900 3200
Connection ~ 4200 3200
$EndSCHEMATC
