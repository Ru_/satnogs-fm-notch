<Qucs Schematic 0.0.19>
<Properties>
  <View=-188,-628,2229,1575,0.526551,0,0>
  <Grid=10,10,1>
  <DataSet=satnogs-fm-notch.dat>
  <DataDisplay=satnogs-fm-notch.dpl>
  <OpenDisplay=1>
  <Script=satnogs-fm-notch.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
  <.ID -20 -16 SUB>
  <Line -20 20 40 0 #000080 2 1>
  <Line 20 20 0 -40 #000080 2 1>
  <Line -20 -20 40 0 #000080 2 1>
  <Line -20 20 0 -40 #000080 2 1>
</Symbol>
<Components>
  <GND * 1 640 800 0 0 0 0>
  <Eqn SWR1 1 30 850 -28 15 0 0 "absS11=abs(S[1,1])" 1 "swr=(1+absS11)/(1-absS11)" 1 "yes" 0>
  <Eqn Eqn1 1 30 760 -28 15 0 0 "dBS21=dB(S[2,1])" 1 "dBS11=dB(S[1,1])" 1 "yes" 0>
  <SUBST FR4 1 30 580 -30 24 0 0 "4.5" 1 "1.6mm" 1 "35 um" 1 "0.02" 1 "1.72e-8" 1 "0" 1>
  <Pac P1 1 510 550 -94 -26 1 1 "1" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 510 800 0 0 0 0>
  <GND * 1 1050 800 0 0 0 0>
  <GND * 1 1440 800 0 0 0 0>
  <MLIN MS1 1 640 650 15 -26 0 1 "FR4" 0 "1.4 mm" 0 "1.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS16 1 1050 660 15 -26 0 1 "FR4" 0 "1.4 mm" 0 "1.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS17 1 1440 660 15 -26 0 1 "FR4" 0 "1.4 mm" 0 "1.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <GND * 1 860 340 0 0 0 0>
  <GND * 1 1270 340 0 0 0 0>
  <MLIN MS5 1 700 340 -26 -72 0 2 "FR4" 0 "3 mm" 0 "3.4mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS7 1 980 340 -26 -72 0 2 "FR4" 0 "3 mm" 0 "2.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS8 1 1110 340 -26 -72 0 2 "FR4" 0 "3 mm" 0 "2.8mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS11 1 1370 340 -26 -72 0 2 "FR4" 0 "3 mm" 0 "2.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <Pac P2 1 2080 580 18 -26 0 1 "2" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <GND * 1 2080 800 0 0 0 0>
  <GND * 1 1720 340 0 0 0 0>
  <MLIN MS20 1 1820 340 -26 -72 0 2 "FR4" 0 "3 mm" 0 "2.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS21 1 1530 340 -26 -72 0 2 "FR4" 0 "3 mm" 0 "2.8mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <GND * 1 1890 800 0 0 0 0>
  <MLIN MS22 1 1890 660 15 -26 0 1 "FR4" 0 "1.4 mm" 0 "1.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS4 1 580 340 -26 -72 0 2 "FR4" 0 "3 mm" 0 "7.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS19 1 1960 340 -26 -72 0 2 "FR4" 0 "3 mm" 0 "7.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <SPfile L2 1 840 260 -26 -59 0 0 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/0603AS022J.S2P" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile C2 1 840 410 -26 40 0 2 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/GRM1885C1H101JA01.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <.SP SP1 1 10 360 0 66 0 0 "log" 1 "10MHz" 1 "2000MHz" 1 "10000" 1 "no" 0 "1" 0 "2" 0 "no" 0 "no" 0>
  <SPfile C3 1 1050 750 -60 -26 0 1 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/GRM1885C1H150JA01.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile L3 1 1050 580 -58 -26 0 1 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/0603ASR18J.S2P" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile L1 1 640 570 -58 -26 0 1 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/LQW18CNR33J00.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile C1 1 640 740 -60 -26 0 1 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/GRM1885C1H6R8DA01.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <GND * 0 670 1530 0 0 0 0>
  <GND * 0 540 1530 0 0 0 0>
  <GND * 0 1080 1530 0 0 0 0>
  <GND * 0 1470 1530 0 0 0 0>
  <MLIN MS23 0 670 1380 15 -26 0 1 "FR4" 0 "1.4 mm" 0 "1.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS24 0 1080 1390 15 -26 0 1 "FR4" 0 "1.4 mm" 0 "1.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS25 0 1470 1390 15 -26 0 1 "FR4" 0 "1.4 mm" 0 "1.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS26 0 730 1070 -26 -72 0 2 "FR4" 0 "3 mm" 0 "3.4mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS27 0 1010 1070 -26 -72 0 2 "FR4" 0 "3 mm" 0 "2.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS28 0 1140 1070 -26 -72 0 2 "FR4" 0 "3 mm" 0 "2.8mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS29 0 1400 1070 -26 -72 0 2 "FR4" 0 "3 mm" 0 "2.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <GND * 0 2110 1530 0 0 0 0>
  <MLIN MS30 0 1850 1070 -26 -72 0 2 "FR4" 0 "3 mm" 0 "2.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS31 0 1560 1070 -26 -72 0 2 "FR4" 0 "3 mm" 0 "2.8mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <GND * 0 1920 1530 0 0 0 0>
  <MLIN MS32 0 1920 1390 15 -26 0 1 "FR4" 0 "1.4 mm" 0 "1.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS33 0 610 1070 -26 -72 0 2 "FR4" 0 "3 mm" 0 "7.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <MLIN MS34 0 1990 1070 -26 -72 0 2 "FR4" 0 "3 mm" 0 "7.8 mm" 1 "Hammerstad" 0 "Kirschning" 0 "26.85" 0>
  <C C9 0 1080 1480 17 -26 0 1 "15pF" 1 "" 0 "neutral" 0>
  <C C10 0 1470 1480 17 -26 0 1 "15pF" 1 "" 0 "neutral" 0>
  <C C11 0 670 1470 17 -26 0 1 "6.8pF" 1 "" 0 "neutral" 0>
  <C C12 0 1920 1470 17 -26 0 1 "6.8pF" 1 "" 0 "neutral" 0>
  <L L9 0 1920 1290 17 -26 0 1 "330nH" 1 "" 0>
  <L L10 0 670 1280 17 -26 0 1 "330nH" 1 "" 0>
  <L L11 0 1080 1290 17 -26 0 1 "180nH" 1 "" 0>
  <L L12 0 1470 1290 17 -26 0 1 "180nH" 1 "" 0>
  <C C13 0 1280 1140 -26 10 0 0 "100pF" 1 "" 0 "neutral" 0>
  <L L13 0 1280 980 -26 -44 0 0 "27nH" 1 "" 0>
  <L L14 0 870 990 -26 -44 0 0 "22nH" 1 "" 0>
  <L L15 0 1730 980 -26 -44 0 0 "22nH" 1 "" 0>
  <C C14 0 870 1140 -26 10 0 0 "100pF" 1 "" 0 "neutral" 0>
  <C C15 0 1730 1140 -26 10 0 0 "100pF" 1 "" 0 "neutral" 0>
  <Pac P3 0 540 1280 -94 -26 1 1 "3" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <Pac P4 0 2110 1310 18 -26 0 1 "4" 1 "50 Ohm" 1 "0 dBm" 0 "1 GHz" 0 "26.85" 0>
  <SPfile L5 1 1440 580 -58 -26 0 1 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/0603ASR18J.S2P" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile C5 1 1440 750 -60 -26 0 1 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/GRM1885C1H150JA01.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile L4 1 1250 250 -26 -59 0 0 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/0603AS027J.S2P" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile C4 1 1250 410 -26 40 0 2 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/GRM1885C1H101JA01.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile L6 1 1700 250 -26 -59 0 0 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/0603AS022J.S2P" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile C6 1 1700 410 -26 40 0 2 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/GRM1885C1H101JA01.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile L7 1 1890 580 -58 -26 0 1 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/LQW18CNR33J00.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
  <SPfile C7 1 1890 750 -60 -26 0 1 "/home/azisi/Documents/SatNOGS/satnogs-fm-notch/qucs/models/GRM1885C1H6R8DA01.s2p" 0 "rectangular" 0 "linear" 0 "open" 0 "2" 0>
</Components>
<Wires>
  <510 580 510 800 "" 0 0 0 "">
  <1400 340 1440 340 "" 0 0 0 "">
  <770 260 810 260 "" 0 0 0 "">
  <770 410 810 410 "" 0 0 0 "">
  <840 290 840 330 "" 0 0 0 "">
  <860 330 860 340 "" 0 0 0 "">
  <840 330 840 380 "" 0 0 0 "">
  <840 330 860 330 "" 0 0 0 "">
  <1180 410 1220 410 "" 0 0 0 "">
  <1180 250 1220 250 "" 0 0 0 "">
  <1280 250 1310 250 "" 0 0 0 "">
  <1280 410 1310 410 "" 0 0 0 "">
  <1250 280 1250 330 "" 0 0 0 "">
  <1270 330 1270 340 "" 0 0 0 "">
  <1250 330 1250 380 "" 0 0 0 "">
  <1250 330 1270 330 "" 0 0 0 "">
  <1010 340 1050 340 "" 0 0 0 "">
  <510 340 510 520 "" 0 0 0 "">
  <510 340 550 340 "" 0 0 0 "">
  <610 340 640 340 "" 0 0 0 "">
  <870 260 910 260 "" 0 0 0 "">
  <870 410 910 410 "" 0 0 0 "">
  <770 260 770 340 "" 0 0 0 "">
  <910 260 910 340 "" 0 0 0 "">
  <910 340 910 410 "" 0 0 0 "">
  <910 340 950 340 "" 0 0 0 "">
  <770 340 770 410 "" 0 0 0 "">
  <730 340 770 340 "" 0 0 0 "">
  <1180 250 1180 340 "" 0 0 0 "">
  <1180 340 1180 410 "" 0 0 0 "">
  <1140 340 1180 340 "" 0 0 0 "">
  <1310 250 1310 340 "" 0 0 0 "">
  <1310 340 1310 410 "" 0 0 0 "">
  <1310 340 1340 340 "" 0 0 0 "">
  <1990 340 2080 340 "" 0 0 0 "">
  <2080 340 2080 550 "" 0 0 0 "">
  <2080 610 2080 800 "" 0 0 0 "">
  <1630 410 1670 410 "" 0 0 0 "">
  <1630 250 1670 250 "" 0 0 0 "">
  <1730 250 1760 250 "" 0 0 0 "">
  <1730 410 1760 410 "" 0 0 0 "">
  <1720 330 1720 340 "" 0 0 0 "">
  <1700 280 1700 330 "" 0 0 0 "">
  <1700 330 1700 380 "" 0 0 0 "">
  <1700 330 1720 330 "" 0 0 0 "">
  <1630 250 1630 340 "" 0 0 0 "">
  <1760 250 1760 340 "" 0 0 0 "">
  <1760 340 1760 410 "" 0 0 0 "">
  <1760 340 1790 340 "" 0 0 0 "">
  <1630 340 1630 410 "" 0 0 0 "">
  <1560 340 1630 340 "" 0 0 0 "">
  <1850 340 1890 340 "" 0 0 0 "">
  <640 770 640 790 "" 0 0 0 "">
  <640 680 640 710 "" 0 0 0 "">
  <640 600 640 620 "" 0 0 0 "">
  <640 340 670 340 "" 0 0 0 "">
  <640 340 640 540 "" 0 0 0 "">
  <670 570 750 570 "" 0 0 0 "">
  <750 570 750 740 "" 0 0 0 "">
  <670 740 750 740 "" 0 0 0 "">
  <640 790 640 800 "" 0 0 0 "">
  <640 790 750 790 "" 0 0 0 "">
  <750 740 750 790 "" 0 0 0 "">
  <1050 780 1050 790 "" 0 0 0 "">
  <1050 690 1050 720 "" 0 0 0 "">
  <1050 610 1050 630 "" 0 0 0 "">
  <1050 340 1080 340 "" 0 0 0 "">
  <1050 340 1050 550 "" 0 0 0 "">
  <1080 750 1150 750 "" 0 0 0 "">
  <1150 580 1150 750 "" 0 0 0 "">
  <1080 580 1150 580 "" 0 0 0 "">
  <1050 790 1050 800 "" 0 0 0 "">
  <1050 790 1150 790 "" 0 0 0 "">
  <1150 750 1150 790 "" 0 0 0 "">
  <1440 340 1500 340 "" 0 0 0 "">
  <1440 340 1440 550 "" 0 0 0 "">
  <1440 780 1440 790 "" 0 0 0 "">
  <1440 690 1440 720 "" 0 0 0 "">
  <1440 610 1440 630 "" 0 0 0 "">
  <1470 580 1540 580 "" 0 0 0 "">
  <1540 580 1540 750 "" 0 0 0 "">
  <1470 750 1540 750 "" 0 0 0 "">
  <1440 790 1440 800 "" 0 0 0 "">
  <1440 790 1540 790 "" 0 0 0 "">
  <1540 750 1540 790 "" 0 0 0 "">
  <1890 780 1890 790 "" 0 0 0 "">
  <1890 690 1890 720 "" 0 0 0 "">
  <1890 610 1890 630 "" 0 0 0 "">
  <1890 340 1930 340 "" 0 0 0 "">
  <1890 340 1890 550 "" 0 0 0 "">
  <1920 750 1990 750 "" 0 0 0 "">
  <1990 580 1990 750 "" 0 0 0 "">
  <1920 580 1990 580 "" 0 0 0 "">
  <1890 790 1890 800 "" 0 0 0 "">
  <1890 790 1990 790 "" 0 0 0 "">
  <1990 750 1990 790 "" 0 0 0 "">
  <540 1310 540 1530 "" 0 0 0 "">
  <800 990 840 990 "" 0 0 0 "">
  <800 1140 840 1140 "" 0 0 0 "">
  <1210 1140 1250 1140 "" 0 0 0 "">
  <1210 980 1250 980 "" 0 0 0 "">
  <1310 980 1340 980 "" 0 0 0 "">
  <1310 1140 1340 1140 "" 0 0 0 "">
  <540 1070 540 1250 "" 0 0 0 "">
  <540 1070 580 1070 "" 0 0 0 "">
  <900 990 940 990 "" 0 0 0 "">
  <900 1140 940 1140 "" 0 0 0 "">
  <940 990 940 1070 "" 0 0 0 "">
  <940 1070 940 1140 "" 0 0 0 "">
  <940 1070 980 1070 "" 0 0 0 "">
  <800 990 800 1070 "" 0 0 0 "">
  <800 1070 800 1140 "" 0 0 0 "">
  <760 1070 800 1070 "" 0 0 0 "">
  <1210 980 1210 1070 "" 0 0 0 "">
  <1210 1070 1210 1140 "" 0 0 0 "">
  <1170 1070 1210 1070 "" 0 0 0 "">
  <1340 980 1340 1070 "" 0 0 0 "">
  <1340 1070 1340 1140 "" 0 0 0 "">
  <1340 1070 1370 1070 "" 0 0 0 "">
  <2020 1070 2110 1070 "" 0 0 0 "">
  <2110 1070 2110 1280 "" 0 0 0 "">
  <2110 1340 2110 1530 "" 0 0 0 "">
  <1660 1140 1700 1140 "" 0 0 0 "">
  <1660 980 1700 980 "" 0 0 0 "">
  <1760 980 1790 980 "" 0 0 0 "">
  <1760 1140 1790 1140 "" 0 0 0 "">
  <1790 980 1790 1070 "" 0 0 0 "">
  <1790 1070 1790 1140 "" 0 0 0 "">
  <1790 1070 1820 1070 "" 0 0 0 "">
  <1660 980 1660 1070 "" 0 0 0 "">
  <1660 1070 1660 1140 "" 0 0 0 "">
  <1590 1070 1660 1070 "" 0 0 0 "">
  <640 1070 670 1070 "" 0 0 0 "">
  <670 1070 700 1070 "" 0 0 0 "">
  <670 1070 670 1250 "" 0 0 0 "">
  <670 1310 670 1350 "" 0 0 0 "">
  <1080 1510 1080 1530 "" 0 0 0 "">
  <1080 1320 1080 1360 "" 0 0 0 "">
  <1080 1420 1080 1450 "" 0 0 0 "">
  <1040 1070 1080 1070 "" 0 0 0 "">
  <1080 1070 1110 1070 "" 0 0 0 "">
  <1080 1070 1080 1260 "" 0 0 0 "">
  <1430 1070 1470 1070 "" 0 0 0 "">
  <1470 1070 1530 1070 "" 0 0 0 "">
  <1470 1070 1470 1260 "" 0 0 0 "">
  <1470 1420 1470 1450 "" 0 0 0 "">
  <1470 1320 1470 1360 "" 0 0 0 "">
  <1470 1510 1470 1530 "" 0 0 0 "">
  <1880 1070 1920 1070 "" 0 0 0 "">
  <1920 1070 1960 1070 "" 0 0 0 "">
  <1920 1070 1920 1260 "" 0 0 0 "">
  <1920 1500 1920 1530 "" 0 0 0 "">
  <1920 1320 1920 1360 "" 0 0 0 "">
  <1920 1420 1920 1440 "" 0 0 0 "">
  <670 1410 670 1440 "" 0 0 0 "">
  <670 1500 670 1530 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Rect 1060 151 962 613 3 #c0c0c0 1 00 1 0 0.2 1 1 -0.1 0.5 1.1 1 -0.1 0.5 1.1 315 0 225 "" "" "">
	<"swr" #0000ff 0 3 0 0 0>
	  <Mkr 1.40339e+08 218 -373 3 0 0>
	  <Mkr 4.37783e+08 281 -303 3 0 0>
	  <Mkr 1.01608e+09 489 -178 3 0 0>
	  <Mkr 8.1421e+08 392 -236 3 0 0>
	  <Mkr 9.99764e+07 136 -444 3 0 0>
  </Rect>
  <Rect 20 145 967 607 3 #c0c0c0 1 00 1 0 0.2 1 1 -0.1 0.5 1.1 1 -0.1 0.5 1.1 315 0 225 "" "" "">
	<"dBS21" #ff0000 0 3 0 0 0>
	  <Mkr 1.05194e+08 141 -419 3 0 0>
	  <Mkr 1.39671e+08 98 -693 3 0 0>
	  <Mkr 9.99764e+07 -188 -467 3 0 0>
	  <Mkr 1.01286e+09 500 -693 3 0 0>
	  <Mkr 4.43856e+08 295 -689 3 0 0>
	  <Mkr 7.04716e+07 -206 -611 3 0 0>
  </Rect>
</Diagrams>
<Paintings>
  <Text 10 240 12 #000000 0 "Band-reject filter\n88MHz...108MHz, PI-type,\nimpedance matching 50 Ohm">
  <Text 4 938 10 #000000 0 "C2, C4, C6 : GRM1885C1H101JA01D 100pF\nC3, C5 : GRM1885C1H150JA01D 15pF\nC1, C7 : GRM1885C1H6R8DA01D 6.8pF\nL2, L6 : 0603AS-022J-YY 22nH\nL4 : 0603AS-027J-YY 27nH\nL1, L7 : LQW18CNR33J00 330nH\nL3, L5  : 0603AS-R18J-YY 180nH">
</Paintings>
